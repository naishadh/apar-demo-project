import React, { Component } from 'react'
import Auth from './Auth'

export default class Hovering extends Component {
    render() {
        return (
            <div>
                <h1>Heyy... {this.props.username}</h1>
                <h3>What's Up?</h3>
                <button onClick={()=> {
                    Auth.logout(()=>{this.props.history.push("/");
                });
                }
                    } >Logout</button>
            </div>
        )
    }
}
